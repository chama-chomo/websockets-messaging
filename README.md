# websockets-messaging

## Description

Playing with messaging using websockets.

## What it does

```
❯ wscat -c localhost:8080
Connected (press CTRL+C to quit)
> hello
< Your message is: hello. Time received : 2023-01-23 21:44:23.516974 +0100 CET m=+6.398639543
```

service

```
❯ go run .
2023/01/23 21:44:17 http listen on 8080
2023/01/23 21:44:22 ws connected!
2023/01/23 21:44:23 publishing message to mq: hello
```

nats-server

```
[84091] 2023/01/23 21:45:23.511165 [TRC] 127.0.0.1:53949 - cid:4 - "v1.23.0:go" - <<- MSG_PAYLOAD: ["hello"]
```
