package main

import (
	"ws_listener/in"
	"ws_listener/out"
	"ws_listener/schema"

	toml "github.com/pelletier/go-toml"
)

type Config struct {
}

type Service struct {
	Config     *schema.Config
	MsgHandler *out.MsgHandler
	WS         *in.WS
}

// Entry point of the service
func (s *Service) Run() error {
	go s.WS.ServeHTTP()

	for {
		select {
		case msg := <-s.WS.WSCh:
			s.MsgHandler.Handle(msg)
		}
	}
}

func main() {
	// currently no config
	toml.LoadFile("config.toml")

	ws := in.NewWS()
	msgHandler := out.NewMsgHandler()

	service := Service{
		WS:         ws,
		MsgHandler: msgHandler,
	}

	service.Run()
}
