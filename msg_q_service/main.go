package main

import (
	"log"
	"ws_listener/in"
	"ws_listener/out"
	"ws_listener/schema"

	"github.com/nats-io/nats.go"
	toml "github.com/pelletier/go-toml"
)

type Config struct {
}

type Service struct {
	Config       *schema.Config
	WSPublisher  *out.WSPublisher
	MQSubscriber *in.MQSubscriber
}

// Entry point of the service
func (s *Service) Run() error {
	go s.WSPublisher.ServeHTTP()

	s.MQSubscriber.Subscribe("Test")
	go s.WSPublisher.HandleWS()

	for {
		select {
		case msg := <-s.MQSubscriber.MQSCh:
			log.Println("received message: ", msg)
			s.WSPublisher.WSPubCh <- msg.Data
		}
	}
}

func main() {
	// currently no config
	toml.LoadFile("config.toml")

	ws := out.NewWSPublisher()
	mq := in.NewMQSubsc(nats.DefaultURL)

	service := Service{
		MQSubscriber: mq,
		WSPublisher:  ws,
	}

	service.Run()
}
